const fs = require('fs-extra')
const { resolve } = require('path')

// Rename: _nuxt to static
fs.moveSync(resolve(__dirname, '../public/_nuxt'), resolve(__dirname, '../public/static'))